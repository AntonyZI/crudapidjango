document.addEventListener("DOMContentLoaded", () => {
    getData();
});

function getData(){
    fetch("http://127.0.0.1:8000/dato/").then(
        res => res.json()
    ).then(response => {
        var tmpData = "";
        console.log(response);
        response.forEach(data => {
            tmpData += "<tr>";
            tmpData += "<td>" + data.DatosId + "</td>";
            tmpData += "<td>" + data.Dato1 + "</td>";
            tmpData += "<td>" + data.Dato2 + "</td>";
            tmpData += "<td>" + data.Dato3 + "</td>";
            tmpData += "<td><button class='btn btn-primary btn-sm' onclick='editDataCall(" + data.DatosId + ")'>Edit</button></td>";
            tmpData += "<td><button class='btn btn-danger btn-sm' onclick='deleteData(" + data.DatosId + ")'>Delete</button></td>";
            tmpData += "</tr>";
        });
        document.querySelector("#tbData").innerHTML = tmpData;
    });
}

var editFormData;
function getFormData(){
    return {
        DatosId: document.querySelector("#DatosId").value,
        Dato1: document.querySelector("#Dato1").value,
        Dato2: document.querySelector("#Dato2").value,
        Dato3: document.querySelector("#Dato3").value
    }
}

function clearFormData(){
    document.querySelector("#DatosId").value = "";
    document.querySelector("#Dato1").value = "";
    document.querySelector("#Dato2").value = "";
    document.querySelector("#Dato3").value = "";
    editFormData = false;
}

function setFormData(DatosId, Dato1, Dato2, Dato3){
    document.querySelector("#DatosId").value = DatosId;
    document.querySelector("#Dato1").value = Dato1;
    document.querySelector("#Dato2").value = Dato2;
    document.querySelector("#Dato3").value = Dato3;
}

function addData(){
    let payload = getFormData();
    console.log(payload);
    fetch("http://127.0.0.1:8000/datoPost/", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(payload)
    })
    .then(res => res.json())
    .then(response => {
        clearFormData();
        getData();
    });
}

function editDataCall(id){
    console.log("Editar", id)
    fetch("http://127.0.0.1:8000/datoById/" + id + "/", {
        method: "GET"
    })
    .then(res => res.json())
    .then(response => {
        console.log("Edit info", response);
        editFormData = response[0];
        setFormData(
            editFormData.DatosId,
            editFormData.Dato1,
            editFormData.Dato2,
            editFormData.Dato3
        );
    });
}

function editData(){
    var formData = getFormData();
    formData.id = editFormData.DatosId;
    fetch("http://127.0.0.1:8000/datoPut/", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
    })
    .then(res => res.json())
    .then(response => {
        clearFormData();
        getData();
    });
}

function deleteData(){
    fetch("http://127.0.0.1:8000/datoDelete/" + id + "/")
    .then(res => res.json())
    .then(response => {
        getData();
    });
}

function submitForm(){
    if(!editFormData) addData(); // Si no hay datos se agrega
    else editData(); // Si hay datos se actualiza
}