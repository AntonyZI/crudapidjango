from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from custom_apps.DatosApp.models import *
from custom_apps.DatosApp.serializers import *

# Create your views here.

@csrf_exempt
def datoApi(request):
    request.method == 'GET'
    datos = Datos.objects.all()
    datos_serializer = DatosSerializer(datos, many=True)
    return JsonResponse(datos_serializer.data, safe=False)

@csrf_exempt
def datoByIdApi(request, pk=0):
    request.method == 'GET'
    datos = Datos.objects.filter(DatosId=pk)
    datos_serializer = DatosSerializer(datos, many=True)
    return JsonResponse(datos_serializer.data, safe=False)

@csrf_exempt
def datoPostApi(request):
    request.method == 'POST'
    dato_data = JSONParser().parse(request)
    datos_serializer = DatosSerializer(data=dato_data)
    if datos_serializer.is_valid():
        datos_serializer.save()
        return JsonResponse("Added Successfully", safe=False)
    return JsonResponse("Failed to Add", safe=False)

@csrf_exempt
def datoPutApi(request, pk=0):
    request.method == 'PUT'
    dato_data = JSONParser().parse(request)
    dato = Datos.objects.get(DatosId=dato_data['DatosId'])
    datos_serializer = DatosSerializer(dato, data=dato_data)
    if datos_serializer.is_valid():
        datos_serializer.save()
        return JsonResponse("Update Successfully", safe=False)
    return JsonResponse("Failed to Update")

@csrf_exempt
def datoDeleteApi(request, pk=0):
    request.method == 'DELETE'
    dato = Datos.objects.get(DatosId=pk)
    dato.delete()
    return JsonResponse("Deleted Successfully", safe=False)
