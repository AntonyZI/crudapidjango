from rest_framework import serializers
from custom_apps.DatosApp.models import *

class DatosSerializer(serializers.ModelSerializer):
    class Meta:
        model=Datos
        fields=('DatosId','Dato1','Dato2','Dato3')
