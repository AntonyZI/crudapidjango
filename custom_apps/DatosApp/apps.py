from django.apps import AppConfig


class DatosappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'custom_apps.DatosApp'
