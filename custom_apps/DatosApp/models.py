from django.db import models

# Create your models here.

class Datos(models.Model):
    DatosId = models.AutoField(primary_key=True)
    Dato1 = models.CharField(max_length=500)
    Dato2 = models.CharField(max_length=500)
    Dato3 = models.CharField(max_length=500)
